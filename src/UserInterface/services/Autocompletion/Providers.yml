parameters:
    autocompletion.largeProviderResultLimit: 15
    autocompletion.finalSuggestionsResultLimit: 50

services:
    keywordAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\KeywordAutocompletionProvider

    applicabilityCheckingKeywordAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@keywordAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@keywordAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    docblockTagAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\DocblockTagAutocompletionProvider
        arguments:
            - '@docblockAutocompletionPrefixDeterminer'

    applicabilityCheckingDocblockTagAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@docblockTagAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@docblockTagAutocompletionApplicabilityChecker'
            - '@docblockAutocompletionPrefixDeterminer'

    superglobalAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\SuperglobalAutocompletionProvider

    applicabilityCheckingSuperglobalAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@superglobalAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@localVariableAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    classAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ClasslikeAutocompletionProvider
        arguments:
            - '@classFilteringClasslikeListProvider'
            - '@useStatementInsertionCreator'
            - '@defaultAutocompletionPrefixDeterminer'
            - '@bestStringApproximationDeterminer'
            - '%autocompletion.largeProviderResultLimit%'

    applicabilityCheckingClassAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@classAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@classAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    interfaceAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ClasslikeAutocompletionProvider
        arguments:
            - '@interfaceFilteringClasslikeListProvider'
            - '@useStatementInsertionCreator'
            - '@defaultAutocompletionPrefixDeterminer'
            - '@bestStringApproximationDeterminer'
            - '%autocompletion.largeProviderResultLimit%'

    applicabilityCheckingInterfaceAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@interfaceAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@interfaceAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    traitAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ClasslikeAutocompletionProvider
        arguments:
            - '@traitFilteringClasslikeListProvider'
            - '@useStatementInsertionCreator'
            - '@defaultAutocompletionPrefixDeterminer'
            - '@bestStringApproximationDeterminer'
            - '%autocompletion.largeProviderResultLimit%'

    applicabilityCheckingTraitAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@traitAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@traitAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    docblockAnnotationAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ClasslikeAutocompletionProvider
        arguments:
            - '@annotationFilteringClasslikeListProvider'
            - '@useStatementInsertionCreator'
            - '@defaultAutocompletionPrefixDeterminer'
            - '@bestStringApproximationDeterminer'
            - '%autocompletion.largeProviderResultLimit%'

    applicabilityCheckingDocblockAnnotationAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@docblockAnnotationAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@docblockTagAutocompletionApplicabilityChecker'
            - '@docblockAutocompletionPrefixDeterminer'

    namespaceAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\NamespaceAutocompletionProvider
        arguments:
            - '@namespaceListProvider'
            - '@defaultAutocompletionPrefixDeterminer'
            - '@bestStringApproximationDeterminer'
            - '%autocompletion.largeProviderResultLimit%'

    applicabilityCheckingNamespaceAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@namespaceAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@namespaceAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    functionAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\FunctionAutocompletionProvider
        arguments:
            - '@functionListProvider'
            - '@defaultAutocompletionPrefixDeterminer'
            - '@bestStringApproximationDeterminer'
            - '@functionAutocompletionSuggestionLabelCreator'
            - '@functionAutocompletionSuggestionParanthesesNecessityEvaluator'
            - '@autocompletionSuggestionTypeFormatter'
            - '%autocompletion.largeProviderResultLimit%'

    applicabilityCheckingFunctionAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@functionAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@functionAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    constantAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ConstantAutocompletionProvider
        arguments:
            - '@constantListProvider'
            - '@defaultAutocompletionPrefixDeterminer'
            - '@bestStringApproximationDeterminer'
            - '@autocompletionSuggestionTypeFormatter'
            - '%autocompletion.largeProviderResultLimit%'

    applicabilityCheckingConstantAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@constantAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@constantAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    nonStaticMethodAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\NonStaticMethodAutocompletionProvider
        arguments:
            - '@expressionTypeDeducer'
            - '@classlikeInfoBuilder'
            - '@functionAutocompletionSuggestionLabelCreator'
            - '@functionAutocompletionSuggestionParanthesesNecessityEvaluator'
            - '@autocompletionSuggestionTypeFormatter'

    applicabilityCheckingNonStaticMethodAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@nonStaticMethodAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@nonStaticMethodAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    staticMethodAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\StaticMethodAutocompletionProvider
        arguments:
            - '@expressionTypeDeducer'
            - '@classlikeInfoBuilder'
            - '@functionAutocompletionSuggestionLabelCreator'
            - '@functionAutocompletionSuggestionParanthesesNecessityEvaluator'
            - '@autocompletionSuggestionTypeFormatter'

    applicabilityCheckingStaticMethodAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@staticMethodAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@staticMethodAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    nonStaticPropertyAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\NonStaticPropertyAutocompletionProvider
        arguments:
            - '@expressionTypeDeducer'
            - '@classlikeInfoBuilder'
            - '@autocompletionSuggestionTypeFormatter'

    applicabilityCheckingNonStaticPropertyAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@nonStaticPropertyAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@nonStaticPropertyAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    staticPropertyAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\StaticPropertyAutocompletionProvider
        arguments:
            - '@expressionTypeDeducer'
            - '@classlikeInfoBuilder'
            - '@autocompletionSuggestionTypeFormatter'

    applicabilityCheckingStaticPropertyAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@staticPropertyAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@staticPropertyAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    classConstantAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ClassConstantAutocompletionProvider
        arguments:
            - '@expressionTypeDeducer'
            - '@classlikeInfoBuilder'
            - '@autocompletionSuggestionTypeFormatter'

    applicabilityCheckingClassConstantAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@classConstantAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@classConstantAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    localVariableAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\LocalVariableAutocompletionProvider
        arguments:
            - '@variableScanner'
            - '@parser'
            - '@autocompletionSuggestionTypeFormatter'
            - '@defaultAutocompletionPrefixDeterminer'

    applicabilityCheckingLocalVariableAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@localVariableAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@localVariableAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    parameterNameAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ParameterNameAutocompletionProvider
        arguments:
            - '@nodeAtOffsetLocator'
            - '@defaultAutocompletionPrefixDeterminer'

    applicabilityCheckingParameterNameAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\ApplicabilityCheckingAutocompletionProvider
        arguments:
            - '@parameterNameAutocompletionProvider'
            - '@nodeAtOffsetLocator'
            - '@parameterNameAutocompletionApplicabilityChecker'
            - '@defaultAutocompletionPrefixDeterminer'

    aggregatingAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\AggregatingAutocompletionProvider
        arguments:
            - '@applicabilityCheckingKeywordAutocompletionProvider'
            - '@applicabilityCheckingDocblockTagAutocompletionProvider'
            - '@applicabilityCheckingDocblockAnnotationAutocompletionProvider'
            - '@applicabilityCheckingSuperglobalAutocompletionProvider'
            - '@applicabilityCheckingClassAutocompletionProvider'
            - '@applicabilityCheckingInterfaceAutocompletionProvider'
            - '@applicabilityCheckingTraitAutocompletionProvider'
            - '@applicabilityCheckingFunctionAutocompletionProvider'
            - '@applicabilityCheckingConstantAutocompletionProvider'
            - '@applicabilityCheckingNamespaceAutocompletionProvider'
            - '@applicabilityCheckingNonStaticMethodAutocompletionProvider'
            - '@applicabilityCheckingStaticMethodAutocompletionProvider'
            - '@applicabilityCheckingNonStaticPropertyAutocompletionProvider'
            - '@applicabilityCheckingStaticPropertyAutocompletionProvider'
            - '@applicabilityCheckingClassConstantAutocompletionProvider'
            - '@applicabilityCheckingLocalVariableAutocompletionProvider'
            - '@applicabilityCheckingParameterNameAutocompletionProvider'

    fuzzyMatchingAutocompletionProvider:
        class: Serenata\Autocompletion\Providers\FuzzyMatchingAutocompletionProvider
        arguments:
            - '@aggregatingAutocompletionProvider'
            - '@defaultAutocompletionPrefixDeterminer'
            - '@bestStringApproximationDeterminer'
            - '%autocompletion.finalSuggestionsResultLimit%'

    autocompletionProvider:
        alias: fuzzyMatchingAutocompletionProvider
