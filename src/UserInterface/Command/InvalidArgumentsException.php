<?php

namespace Serenata\UserInterface\Command;

use DomainException;

/**
 * Exception that indicates invalid arguments were passed for a command.
 */
final class InvalidArgumentsException extends DomainException
{

}
