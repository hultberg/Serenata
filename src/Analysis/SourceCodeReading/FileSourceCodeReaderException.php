<?php

namespace Serenata\Analysis\SourceCodeReading;

use RuntimeException;

/**
 * Indicates something went wrong whilst reading source code for a file.
 */
final class FileSourceCodeReaderException extends RuntimeException
{

}
